FROM openjdk:8-jre-alpine
COPY /target/logs-*.jar logs.jar
CMD ["java", "-jar", "logs.jar"]