Logs é o micro serviço responsável por receber os logs dos serviços de Tabelodromo.

Tópico:
spec2-logs

Grupo:
log-1

Pacote:
br.com.mastertech.producer

Classe Model:
Log.java

Repositório:
https://gitlab.com/itau-spec-2/projeto-final/logs

JSON de entrada via Kafka:
{
"dateTime": "timestamp_sendo_uma_variavel_date",
"env": "ambiente",
"service": "microsserviço_executado",
"classExec": "classe_executada",
"method": "metodo_executado",
"key": "chave_do_sistema_para_pesquisa",
"log": "log_da_execucao"
}
