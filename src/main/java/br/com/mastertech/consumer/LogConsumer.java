package br.com.mastertech.consumer;

import br.com.mastertech.producer.Log;
import com.opencsv.bean.StatefulBeanToCsv;
import com.opencsv.bean.StatefulBeanToCsvBuilder;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileWriter;

@Component
public class LogConsumer {

    @KafkaListener(topics = "spec2-logs", groupId = "log-1")
    public void receber(@Payload Log log) {

        System.out.println("Recebido log do micro serviço " + log.getService());
        File arq = new File("log-tabelodromo.csv");

        try {
            FileWriter fileWriter = new FileWriter(arq, true);
            StatefulBeanToCsv<Log> beanToCsv = new StatefulBeanToCsvBuilder(fileWriter).build();
            beanToCsv.write(log);

            fileWriter.flush();
            fileWriter.close();

        } catch (Exception e){
            System.out.println("Erro: " + e.getClass());
        }

    }

}
