package br.com.mastertech.producer;

import java.sql.Timestamp;

public class Log {

    private Timestamp dateTime;
    private String env;
    private String service;
    private String classExec;
    private String method;
    private String key;
    private String log;

    public Timestamp getDateTime() {
        return dateTime;
    }

    public void setDateTime(Timestamp dateTime) {
        this.dateTime = dateTime;
    }

    public String getEnv() {
        return env;
    }

    public void setEnv(String env) {
        this.env = env;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public String getClassExec() {
        return classExec;
    }

    public void setClassExec(String classExec) {
        this.classExec = classExec;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log = log;
    }

}